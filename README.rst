============
 Ytdlhelper
============

A CLI utility to help with using ``youtube-dl``'s base functionality without resorting to a clunky GUI.

Installation
============

1. Download ``youtube-dl.exe`` from https://rg3.github.io/youtube-dl/download.html
2. Download or build ``ytdlhelper.exe``.
3. Put the executables in the same directory.

Usage
=====

Mosty self-evident but...

:1:
   Enter the YouTube ID

:2:
   Continue any previous downloads in the currecnt directory
