package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"time"
)

var (
	bufStdin *bufio.Reader
)

func init() {
	bufStdin = bufio.NewReader(os.Stdin)
}

type Menu map[rune]string

func (menu Menu) String() string {
	var ret string
	for k, v := range menu {
		ret += string(k) + " - " + v + "\n"
	}
	return ret
}

var mainMenu Menu = map[rune]string{
	'1': "Youtube ID",
	'2': "Continue Parts",
	'3': "Update youtube-dl",
	'q': "Quit"}

func readCurDir() []string {
	d, err := os.Open(".")
	if err != nil {
		panic(err)
	}
	defer d.Close()
	fileNames, err := d.Readdirnames(0)
	if err != nil {
		panic(err)
	}
	return fileNames
}

func partsHere() <-chan []string {
	chout := make(chan []string)
	go func() {
		defer close(chout)
		fileNames := readCurDir()
		fileNameRe := regexp.MustCompile(`-([\w\d-]{11})\.mp4\.part`)
		for _, fileName := range fileNames {
			match := fileNameRe.FindStringSubmatch(fileName)
			if len(match) < 2 {
				continue
			}
			chout <- match
		}
	}()
	return chout
}

func runYouTubeExe(ytID string) error {
	cmd := exec.Command("./youtube-dl.exe", ytID)
	if ytID[0] == '-' {
		if ytID != "-U" {
			cmd.Args[len(cmd.Args)-1] = "--"
			cmd.Args = append(cmd.Args, ytID)
		}
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func getChoice() byte {
	bufStdin.Discard(bufStdin.Buffered())
	fmt.Print(": ")
	c, err := bufStdin.ReadByte()
	if err != nil {
		panic(err)
	}
	bufStdin.Discard(bufStdin.Buffered())
	return c
}

func getLine() string {
	bufStdin.Discard(bufStdin.Buffered())
	ret, err := bufStdin.ReadString('\n')
	if err != nil {
		panic(err)
	}
	return ret
}

func processChoice(choice byte) bool {
	switch choice {
	case '1':
		fmt.Print("Youtube ID: ")
		ytID := getLine()
		if err := runYouTubeExe(ytID); err != nil {
			fmt.Println(err)
		}

	case '2':
		for match := range partsHere() {
			fmt.Println("Trying to continue", match[1])
			if err := runYouTubeExe(match[1]); err != nil {
				fmt.Println(err)
			}
		}
	case '3':
		if err := runYouTubeExe("-U"); err != nil {
			fmt.Println(err)
		}
	case 'q', 'Q':
	default:
		fmt.Println("Bad Choice")
		return false
	}
	return true
}

func hasYoutubeDl() bool {
	fileNames := readCurDir()
	for _, fileName := range fileNames {
		if fileName == "youtube-dl.exe" {
			return true
		}
	}
	return false
}

func main() {
	if !hasYoutubeDl() {
		fmt.Println("Can't find youtube-dl.exe")
		return
	}
	for done := false; !done; {
		fmt.Print(mainMenu)
		done = processChoice(getChoice())
	}
	if runtime.GOOS == "windows" {
		time.Sleep(5 * time.Second)
	}
	fmt.Println("Love you")
	time.Sleep(200 * time.Millisecond)
}
